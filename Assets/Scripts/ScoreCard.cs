﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Endgine;
using Doozy.Engine.UI;
using System;
using DG.Tweening;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class ScoreCard : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public ScoreManager scoreManager;

    public List<Sprite> spriteList =
        new List<Sprite>();

    public UIButton button;

    public CanvasGroup grayCardCanvasGroup;

    public CanvasGroup orangeCardCanvasGroup;

    public int step;

    public int Step
    {
        get 
        {
            return step; 
        }
        set 
        {
            step = Mathf.Clamp(value, 0, scoreManager.numPlayerPerTeam);
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    [Button]
    public void InitFields(string grayCardName = "[Image_Gray]", 
                           string orangeCardName = "[Image_Orange]")
    {
        button = transform.GetComponent<UIButton>();

        foreach (Transform child in transform)
        {
            if(child.name == grayCardName)
            {
                grayCardCanvasGroup = child.GetComponent<CanvasGroup>();
                var img = child.GetComponent<Image>();
                img.sprite = spriteList[0];
            }

            if (child.name == orangeCardName)
            {
                orangeCardCanvasGroup = child.GetComponent<CanvasGroup>();
                orangeCardCanvasGroup.alpha = 0.0f;
                var img = child.GetComponent<Image>();
                img.sprite = spriteList[1];
            }
        }
        if(!scoreManager.scoreCards.Contains(this))
        {
            scoreManager.scoreCards.Add(this);
            int index = scoreManager.scoreCards.IndexOf(this);
            transform.name =
                "[ScoreCard_{0}]".FormatWith((index + 1).ToString("00"));
        }
    }

    public void OnScoreCardClicked()
    {
        if(Step != scoreManager.numPlayerPerTeam)
        {
            StepOrangeOpacity();
            if (Step == scoreManager.numPlayerPerTeam)
                scoreManager.Score++;
        }
    }

    public void StepOrangeOpacity()
    {
        Step++;

        if (Step == scoreManager.numPlayerPerTeam)
        {
            orangeCardCanvasGroup
                .DOFade(1, scoreManager.baseDuration);
            //scoreManager.Score++;
        }
        else
        {
            float toVal =
                Step * (1.0f / scoreManager.numPlayerPerTeam);
            print("toVal: {0}".FormatWith(toVal));
            orangeCardCanvasGroup
                .DOFade(toVal, scoreManager.baseDuration);
        }


    }

    #endregion

}
