﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using BN512.Extensions;
using System;
using System.IO;
using Firebase.Database;
using Doozy.Engine.UI;

[System.Serializable]
public struct NormalQuizzEntry
{
    [SerializeField] 
    public string questionText;
    [SerializeField] 
    public string answearVariant_A;
    [SerializeField] 
    public string answearVariant_B;
    [SerializeField] 
    public string answearVariant_C;
    [SerializeField] 
    public string answearVariant_D;
    [SerializeField] 
    public string correctAnswear;
    [SerializeField] 
    public string reference;

}

[System.Serializable]
public class SpecialQuizzEntry
{
    [SerializeField]
    public int subTeamID;
    [SerializeField] 
    public string questionText;
    [SerializeField] 
    public string answearVariant_A;
    [SerializeField] 
    public string answearVariant_B;
    [SerializeField] 
    public string answearVariant_C;
    [SerializeField] 
    public string answearVariant_D;
    [SerializeField] 
    public string correctAnswear;
    [SerializeField] 
    public int imageIndex;
}

[System.Serializable]
public class QuizzEntriesData
{
    [SerializeField]
    public List<NormalQuizzEntry> normalDeck =
        new List<NormalQuizzEntry>();

    [SerializeField]
    public List<SpecialQuizzEntry> specialDeck =
        new List<SpecialQuizzEntry>();

}

[System.Serializable]
public class QuizzEntriesFile
{
    [SerializeField]
    GameMode gameMode;

    [SerializeField]
    private QuizzEntriesData data;

    public bool isLoaded = false;

    public string fileName = "QuizzEntries.json";

    public QuizzEntriesData Data 
    {
        get
        {
            bool succes = true;
            if (!isLoaded) { succes = LoadData(); }
            return succes ? data : null;
        }
    }

    public string filePath
    {
        get
        {
            return Path.Combine(Application.streamingAssetsPath, fileName);
        }
    }

    [Button(ButtonSizes.Medium)]
    public bool LoadData()
    {
        
        if(gameMode.config.Data.sessionType != SessionType.Quizz_Offline &&
           gameMode.isFirebaseReady)
        {
            var root = gameMode.databaseRoot.Child("quizzEntries");
            root.GetValueAsync().ContinueWith((task) =>
            {
                if(task.IsCompleted)
                {
                    string dataAsJSON = task.Result.GetRawJsonValue();
                    JsonUtility.FromJsonOverwrite(dataAsJSON, data);
                    Debug.Log("[Data Loaded]:\n{0}".FormatWith(dataAsJSON));
                    isLoaded = true;
                }
            });
            return true;
        }

        if (File.Exists(filePath))
        {
            string dataAsJSON = File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(dataAsJSON, data);

            Debug.Log("[Data Loaded]:\n{0}".FormatWith(dataAsJSON));
            isLoaded = true;
            return true;
        }
        else
        {
            Debug.Log("Data file not found.");
            return false;
        }

    }

    [Button(ButtonSizes.Medium)]
    public bool SaveData()
    {
        if (gameMode.config.Data.sessionType != SessionType.Quizz_Offline &&
            gameMode.isFirebaseReady)
        {
            var root = gameMode.databaseRoot.Child("quizzEntries");
            root.SetRawJsonValueAsync(JsonUtility.ToJson(data));
            return true;
        }

        if (File.Exists(filePath))
        {
            string dataAsJSON = JsonUtility.ToJson(data, true);
            File.WriteAllText(filePath, dataAsJSON);

            Debug.Log("[Config Saved]:\n{0}".FormatWith(dataAsJSON));
            return true;
        }
        else
        {
            Debug.Log("Config file not found.");
            return false;
        }
    }
}

public struct VariantState
{
    public Color label;
    public Color panel;
}

public class CorrectAnswear { public int questionIndex; public string sessionID; }

/// <summary>
/// 
/// </summary>
public class DeckManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public QuizzEntriesFile quizzEntries;

    public NormalTextQuizz normalTextQuizz;

    public SpecialTextQuizz specialTextQuizz;

    public SpecialImageQuizz specialImageQuizz;

    public QuestionCard currentCard;

    public VariantState normalAnswear;

    public VariantState rightAnswear;

    public VariantState wrongAnswear;

    public float baseDuration = 0.5f;

    public int numLoops = 4;

    public  int answeardQuestions = 0;

    public List<QuestionCard> questionCards;

    public UIView endscreen;

    public CanvasGroup normalCards;

    public CanvasGroup specialCards;

    public int correctAnswears;

    public EndScreenViewController endScreenViewController;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }

        if(gameMode.config.Data.sessionType == SessionType.Quizz_Offline)
        {
            normalCards.interactable =
                answeardQuestions < gameMode.config.Data.maxScore - 1;
            specialCards.interactable =
                answeardQuestions == gameMode.config.Data.maxScore - 1;
        }
    
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
    
    }

    #endregion

    #region Methods

    public void LoadQuizzEntry(QuestionCard card)
    {

        switch (card.questionType)
        {
            case QuestionType.Normal:
                var normalQuizzEntry =
                    quizzEntries.Data.normalDeck.RemoveRandom();
                normalTextQuizz.LoadQuizzEntry(normalQuizzEntry);
                break;
            case QuestionType.Special:

                if (gameMode.config.Data.sessionType != SessionType.Quizz_Offline)
                {
                    var subTeamID = gameMode.config.Data.subTeamID;
                    quizzEntries.Data.specialDeck.RemoveAll((obj) => obj.subTeamID != subTeamID);
                }

                var specialQuizzEntry =
                    quizzEntries.Data.specialDeck.RemoveRandom();
                if (specialQuizzEntry.imageIndex == -1)
                {
                    specialTextQuizz.LoadQuizzEntry(specialQuizzEntry);
                }
                else
                {
                    specialImageQuizz.LoadQuizzEntry(specialQuizzEntry);
                }
                
                break;
        }

        currentCard = card;
    }


    public void RegisterAnswear(bool validity)
    {
        answeardQuestions++;

        if (validity) correctAnswears++;

        if (gameMode.config.Data.sessionType != SessionType.Quizz_Offline)
        {
            string teamName =
                "Team_{0}".FormatWith(gameMode.config.Data.teamID.ToString("00"));
            CorrectAnswear correctAnswear =
                new CorrectAnswear()
                {
                    questionIndex = currentCard.questionIndex,
                    sessionID = gameMode.config.Data.getSessionID()
                };

            gameMode.databaseRoot
                    .Child("answeardQuestion")
                    .Child(teamName)
                    .Push()
                    .SetRawJsonValueAsync(JsonUtility.ToJson(correctAnswear));
            

            if (answeardQuestions == questionCards.Count)
            {
                endscreen.Show();
            }
        }

        if (gameMode.config.Data.sessionType == SessionType.Quizz_Offline)
        {
            normalCards.interactable =
                answeardQuestions < gameMode.config.Data.maxScore - 1;
            specialCards.interactable =
                answeardQuestions == gameMode.config.Data.maxScore - 1;
        }

        if (answeardQuestions == gameMode.config.Data.maxScore)
        {
            endscreen.Show();
            endScreenViewController.UpdateEndscreenScore(correctAnswears);
        }



    }

    public void UpdateViewWithSessionData()
    {
        /*
        if(gameMode.isFirebaseReady)
        {

            string teamName =
            "Team_{0}".FormatWith(gameMode.config.Data.teamID.ToString("00"));
            gameMode.databaseRoot
                .Child("answeardQuestion")
                    .Child(teamName)
                    .GetValueAsync()
                    .ContinueWith((task) =>
                    {
                        if (task.IsCompleted)
                        {
                            questionCards.ForEach(
                                (questionCard) => questionCard.MoveToInitalPosition());
                            answeardQuestions = 0;
                            print("UpdateViewWithSessionData");
                            foreach (var child in task.Result.Children)
                            {
                                CorrectAnswear correctAnswear = new CorrectAnswear();
                                string correctAnswearAsJSON = child.GetRawJsonValue();
                                print(correctAnswearAsJSON);
                                JsonUtility.FromJsonOverwrite(correctAnswearAsJSON, correctAnswear);

                                if(correctAnswear.sessionID == gameMode.config.Data.getSessionID())
                                {
                                    questionCards[correctAnswear.questionIndex].MoveOffScreen();
                                    answeardQuestions++;
                                }

                            }
                        }

                    });

        }
        */
    }

    public bool ShouldBeSelectable(QuestionCard questionCard)
    {
        if (gameMode.config.Data.sessionType == SessionType.Quizz_Offline)
        {
            if(questionCard.questionType == QuestionType.Normal)
            {
                return answeardQuestions < gameMode.config.Data.maxScore - 1;
            }
            else if(questionCard.questionType == QuestionType.Special)
            {
                return answeardQuestions == gameMode.config.Data.maxScore - 1;
            }
        }
        return true;
    }

    #endregion

}
