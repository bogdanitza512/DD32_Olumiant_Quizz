﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Doozy.Engine.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class SpecialTextQuizz : SerializedMonoBehaviour
{

    #region Nested Types

    public struct AnswearVariant
    {
        public TMP_Text label;
        public Image panel;
    }


    #endregion

    #region Fields and Properties

    public DeckManager deckManager;

    public UIView view;

    public TMP_Text questionLabel;

    public Dictionary<string, AnswearVariant> answearVariants =
        new Dictionary<string, AnswearVariant>()
    {
        {"A", new AnswearVariant()},
        {"B", new AnswearVariant()},
        {"C", new AnswearVariant()},
        {"D", new AnswearVariant()}
    };

    public CountdownTimer countdown;

    public PointsProgressBar pointsProgressBar;

    public bool hasBeenAnsweard = false;

    public string correctAnswear;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {

    }

    #endregion

    #region Methods

    public void LoadQuizzEntry(SpecialQuizzEntry entry)
    {

        hasBeenAnsweard = false;

        questionLabel.text = entry.questionText;
        answearVariants["A"].label.text = entry.answearVariant_A;
        answearVariants["B"].label.text = entry.answearVariant_B;
        answearVariants["C"].label.text = entry.answearVariant_C;
        answearVariants["D"].label.text = entry.answearVariant_D;
        correctAnswear = entry.correctAnswear;
        view.Show();
        countdown.StartCountdown();
        pointsProgressBar.FillStep = deckManager.correctAnswears;

    }

    public void OnAnswearVariantSelected(string variant)
    {
        if (hasBeenAnsweard) return;
        hasBeenAnsweard = true;

        countdown.StopCountdown();

        if (variant == correctAnswear)
        {


            DOTween.Sequence()
                   .OnStart(() =>
                   {
                       pointsProgressBar.FillStep++;
                   })
                   .Append(answearVariants[variant]
                           .panel
                           .DOColor(deckManager.rightAnswear.panel,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))
                   .Join(answearVariants[variant]
                           .label
                           .DOColor(deckManager.rightAnswear.label,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))
                   .OnComplete(() =>
                   {
                       deckManager.RegisterAnswear(variant == correctAnswear);
                       view.Hide();
                   });
        }
        else
        {
            DOTween.Sequence()
                   .Append(answearVariants[variant]
                           .panel
                           .DOColor(deckManager.wrongAnswear.panel,
                                    deckManager.baseDuration / 2)
                           .SetLoops(deckManager.numLoops,
                                     LoopType.Yoyo))
                   .Join(answearVariants[variant]
                           .label
                           .DOColor(deckManager.wrongAnswear.label,
                                    deckManager.baseDuration / 2)
                           .SetLoops(deckManager.numLoops,
                                     LoopType.Yoyo))
                   .Join(answearVariants[correctAnswear]
                           .panel
                           .DOColor(deckManager.rightAnswear.panel,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))
                   .Join(answearVariants[correctAnswear]
                           .label
                           .DOColor(deckManager.rightAnswear.label,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))

                   .OnComplete(() =>
                   {
                       deckManager.RegisterAnswear(variant == correctAnswear);
                       view.Hide();
                   });
        }
    }

    public void ResetAnswearVariantColor()
    {
        foreach (var variant in answearVariants.Values)
        {
            variant.label.color = deckManager.normalAnswear.label;
            variant.panel.color = deckManager.normalAnswear.panel;
        }
    }

    public void OnCountdownFinished()
    {
        hasBeenAnsweard = true;

        DOTween.Sequence()
               .Append(answearVariants[correctAnswear]
                           .panel
                           .DOColor(deckManager.rightAnswear.panel,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))
               .Join(answearVariants[correctAnswear]
                           .label
                           .DOColor(deckManager.rightAnswear.label,
                                    deckManager.baseDuration)
                           .SetLoops(deckManager.numLoops + 1,
                                     LoopType.Yoyo))
                   .OnComplete(() =>
                   {
                       //deckManager.RegisterCorrectAnswear();
                       view.Hide();
                   });

    }

    #endregion

}
