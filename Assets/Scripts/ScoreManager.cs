﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using Firebase.Database;
using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class ScoreManager : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    private string[] customTeamIDs = { "Team_01", "Team_02", "Team_03" };

    [ValueDropdown("customTeamIDs")]
    public string teamID;

    public List<ScoreCard> scoreCards;

    public int score = 0;

    public TMP_Text scoreLabel;

    public int Score
    {
        get { return score; }
        set
        {
            if (value != score)
            {
                score = value;
                scoreLabel.text = score.ToString();
            }
        }
    }

    public Dictionary<int, List<string>> answeardQustions =
        new Dictionary<int, List<string>>();

    public int numPlayerPerTeam = 3;
    public bool hasBeenInitialized = false;
    public float baseDuration = 0.5f;
    public float delay = 1.0f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void InitializeListeners()
    {
        if (gameMode.isFirebaseReady && !hasBeenInitialized)
        {
            var teamAnswearedQuestions =
                gameMode.databaseRoot
                        .Child("answeardQuestion")
                        .Child(teamID);

            teamAnswearedQuestions.ChildAdded += HandleChildAdded;
            teamAnswearedQuestions.ChildRemoved += HandleChildRemoved;
            print("Listeners initialized...");
            hasBeenInitialized = true;
        }

    }

    public void OnShowAnimationStarted()
    {
        DOTween.Sequence()
               .AppendInterval(delay)
               .OnComplete(() => InitializeListeners());
    }


    void HandleChildAdded(object sender, ChildChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        // Do something with the data in args.Snapshot

        string correctAnswearAsJSON = args.Snapshot.GetRawJsonValue();
        CorrectAnswear correctAnswear = new CorrectAnswear();

        print(correctAnswearAsJSON);
        JsonUtility.FromJsonOverwrite(correctAnswearAsJSON, correctAnswear);

        if(answeardQustions.ContainsKey(correctAnswear.questionIndex))
        {
            answeardQustions[correctAnswear.questionIndex]
                    .Add(correctAnswear.sessionID);
        }
        else
        {
            answeardQustions
                .Add(correctAnswear.questionIndex,
                     new List<string>() { correctAnswear.sessionID });
        }

        scoreCards[correctAnswear.questionIndex].StepOrangeOpacity();

        if(answeardQustions[correctAnswear.questionIndex].Count == numPlayerPerTeam)
        {
            // questionCards[correctAnswear.questionIndex].toggle.ToggleOn();
            Score++;
            // print("Score updated: {0}".FormatWith(score));
        }
    }


    void HandleChildRemoved(object sender, ChildChangedEventArgs args)
    {
        print("Child removed");
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }

        // Do something with the data in args.Snapshot

        CorrectAnswear correctAnswear = new CorrectAnswear();

        JsonUtility.FromJsonOverwrite(args.Snapshot.GetRawJsonValue(), correctAnswear);

        if (answeardQustions.ContainsKey(correctAnswear.questionIndex))
        {
            if (answeardQustions[correctAnswear.questionIndex]
                    .Contains(correctAnswear.sessionID))
            {
                answeardQustions[correctAnswear.questionIndex]
                    .Remove(correctAnswear.sessionID);
            }
        }

        if (answeardQustions[correctAnswear.questionIndex].Count < numPlayerPerTeam)
        {
            //questionCards[correctAnswear.questionIndex].toggle.ToggleOff();
            Score--;
            print("Score updated: {0}".FormatWith(score));

        }
    }

    #endregion

}
