﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Doozy.Engine.UI;
using TMPro;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class EndScreenViewController : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public Image profileCircle;

    public List<Sprite> profileCircles = 
        new List<Sprite>();

    public float restartDuration = 5.0f;

    public UIButton restartButton;

    public TMP_Text endscreenLabel;

    public string endscreenTemplate;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }
    
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void UpdateViewWithSessionData()
    {
        if(gameMode.config.Data.sessionType != SessionType.Quizz_Offline)
        {
            profileCircle.sprite = profileCircles[gameMode.config.Data.teamID - 1];
        }
    }

    public void OnEndScreenShowAnimFinished()
    {
        DOTween
            .Sequence()
            .AppendInterval(restartDuration)
            .OnComplete(() =>
            {
                if(restartButton != null)
                {
                    restartButton.ExecuteClick();
                }
            });
    }

    public void UpdateEndscreenScore(int score)
    {
        endscreenLabel.text = endscreenTemplate.FormatWith(score);
    }

    #endregion

}
