﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using Doozy.Engine.UI;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class DebugViewController : SerializedMonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public MasterViewController masterViewController;

    public Dictionary<SessionType, UIToggle> sessionTypeToggleDict =
        new Dictionary<SessionType, UIToggle>()
    {
        {SessionType.Quizz, null},
        {SessionType.Scoreboard, null}
    };

    public List<UIToggle> teamToggles;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void UpdateViewWithSessionData()
    {
        sessionTypeToggleDict[gameMode.config.Data.sessionType].ToggleOn();
        int teamIDIndex =
            (gameMode.config.Data.teamID - 1) * 3 + 
            (gameMode.config.Data.subTeamID - 1);
        teamToggles[teamIDIndex].ToggleOn();
    }


    public void OnToggledSessionType(int selection)
    {
        gameMode.config.Data.sessionType = (SessionType)selection;
        Debug.Log("Session Type: {0}".FormatWith((SessionType)selection));
    }

    public void OnToggledTeam(int selection)
    {
        gameMode.config.Data.teamID = (selection / 3) + 1;
        gameMode.config.Data.subTeamID = (selection % 3) + 1;

        Debug.Log("Team Number: {0}\nSubTeam Number: {1}"
                    .FormatWith((selection / 3 + 1),
                                (selection % 3 + 1)));
    }

    public void OnSaveButton_Clicked()
    {
        gameMode.config.Data.showDebugViewOnStart = false;
        gameMode.config.SaveConfigData();
        masterViewController.ShowSessionView();
    }

    #endregion

}
