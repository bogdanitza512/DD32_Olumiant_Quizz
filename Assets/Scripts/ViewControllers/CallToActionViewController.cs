﻿using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class CallToActionViewController : MonoBehaviour 
{

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public TMP_Text teamLabel;

    public string teamLabelTemplate = "Echipa {0}";

    public Image avatarImage;

    public List<Sprite> avatarList;



    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }
    
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void UpdateViewWithSessionData()
    {
        if (gameMode.config.Data.sessionType != SessionType.Quizz_Offline)
        {
            int teamID = gameMode.config.Data.teamID;
            teamLabel.text = teamLabelTemplate.FormatWith(teamID);
            //avatarImage.sprite = avatarList[teamID - 1];
        }
    }

    #endregion

}
