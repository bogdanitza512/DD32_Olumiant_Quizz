﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class MasterViewController : SerializedMonoBehaviour {
     

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public float showViewDelay = 0.25f;

    public UIView debugView;

    public Dictionary<SessionType, UIView> sessionViewDict
        = new Dictionary<SessionType, UIView>()
    {
        {SessionType.Quizz, null},
        {SessionType.Scoreboard, null}
    };

    UIView currentSessionView
    {
        get
        {
            return sessionViewDict[gameMode.config.Data.sessionType];
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        if(gameMode == null)
        {
            gameMode = GameMode.Instance;
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnMasterViewFinishShowAnim()
    {
        if (gameMode.config.Data.showDebugViewOnStart)
        {
            ShowDebugView();
        }
        else
        {
            ShowSessionView();
        }
    }

    public void ShowDebugView()
    {
        DOTween.Sequence()
               .OnStart(() => currentSessionView.Hide())
               .AppendInterval(showViewDelay)
               .OnComplete(() => debugView.Show())
               .Play();
    }

    public void ShowSessionView()
    {

        DOTween.Sequence()
               .OnStart(() => debugView.Hide())
               .AppendInterval(showViewDelay)
               .OnComplete(() => currentSessionView.Show())
               .Play();
    }

    #endregion

}
