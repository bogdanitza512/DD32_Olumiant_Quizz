﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using Doozy.Engine.UI;
using System;

public enum QuestionType { Normal, Special }

/// <summary>
/// 
/// </summary>
public class QuestionCard : SerializedMonoBehaviour 
{

    #region Nested Types



    #endregion

    #region Fields and Properties
    
    public int questionIndex;
    public QuestionType questionType;
    public UIView view;

    public DeckManager deckManager;

    public bool hasBeenAnsweard = false;

    public Vector3 initialPosition;
    public int initialSiblingIndex;
    public Transform offscreen;
    public float moveOffscreenDuration = 1.0f;

    public UIToggle toggle;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void OnCardSelected()
    {
        //if (!deckManager.ShouldBeSelectable(this)) return;

        view.RectTransform
            .DOMove(offscreen.position, moveOffscreenDuration)
            .OnStart(view.transform.SetAsLastSibling)
            .OnComplete(() =>
        {
            view.Hide(true);
            deckManager.LoadQuizzEntry(this);
        });
    }

    public void MoveOffScreen()
    {
        view.RectTransform
            .DOMove(offscreen.position, moveOffscreenDuration)
            .OnStart(view.transform.SetAsLastSibling)
            .OnComplete(() => view.Hide(true));
    }

    [Button]
    public void FindCardToggle()
    {
        foreach(Transform child in transform)
        {
            var _toggle = child.GetComponent<UIToggle>();
            if(_toggle != null)
            {
                toggle = _toggle;
            }
        }
    }

    [Button]
    public void SetInitialTransform()
    {
        initialPosition = transform.position;
        initialSiblingIndex = transform.GetSiblingIndex();
    }

    public void MoveToInitalPosition()
    {
        view.RectTransform
            .DOMove(offscreen.position, moveOffscreenDuration / 2)
            .OnStart(() => view.Show(true))
            .OnComplete(() => transform.SetSiblingIndex(initialSiblingIndex));

    }

    #endregion

}
