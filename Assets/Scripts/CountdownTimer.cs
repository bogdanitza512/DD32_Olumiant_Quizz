﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using BN512.Extensions;

/// <summary>
/// 
/// </summary>
public class CountdownTimer : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public TMP_Text countdownLabel;

    public Image countdownFill;

    public CanvasGroup canvasGroup;

    public int duration = 60;
    public float strength = 2.5f;

    [SerializeField]
    int currentTime = 0;

    public int CurrentTime
    {
        get { return currentTime; }
        set
        {
            if (value != currentTime)
            {
                currentTime = value;
                countdownLabel.text = currentTime.ToString();
            }
        }
    }

    Sequence countdownSequence;

    public UnityEvent onCountdownFinished;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods

    public void StartCountdown()
    {
        countdownFill.fillAmount = 1;
        CurrentTime = duration;

        countdownSequence =
        DOTween.Sequence()
               .Append(countdownFill
                            .DOFillAmount(0, duration)
                            .SetEase(Ease.Linear))
               .Join(DOTween.To(() => { return CurrentTime; },
                                (x) => { CurrentTime = x; },
                                0,
                                duration)
                            .SetEase(Ease.Linear))
               .AppendCallback(Shake)
               //.OnStart(() => { currentTime = duration; })
               .OnComplete(onCountdownFinished.Invoke);
    }

    public void StopCountdown()
    {
        if (countdownSequence != null && countdownSequence.IsPlaying())
            countdownSequence.Kill();
        Blink();
    }

    public void Blink()
    {
        canvasGroup.DOFade(0, .25f)
                   .SetLoops(2, LoopType.Yoyo);
    }

    public void Shake()
    {
        transform.DOShakePosition(0.5f,
                                  new Vector3().With(x: strength,
                                                     y: strength));
    }




    #endregion

}
