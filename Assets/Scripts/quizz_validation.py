import json

#Special Entries: 9

normal_deck_json = "StreamingAssets/QuizzEntries_NormalDeck.json"
special_deck_json = "StreamingAssets/QuizzEntries_SpecialDeck.json"
output = "StreamingAssets/QuizzEntries.json"
with open(normal_deck_json, encoding='utf-8') as input:
    normal_deck_data = json.load(input)
with open(special_deck_json, encoding='utf-8') as input:
    special_deck_data = json.load(input)

to_lowercase = lambda string: string[:1].lower() + string[1:] if string else ""

for entry in normal_deck_data:
    if "Timestamp" in entry:
        del entry["Timestamp"]


for entry in special_deck_data:
    if "Timestamp" in entry:
        del entry["Timestamp"]
    if not "imageIndex" in entry:
        entry["imageIndex"] = -1

quizz_entries = {
    "normalDeck" : normal_deck_data,
    "specialDeck" : special_deck_data
}

with open(output, "w+", encoding='utf-8') as output:
    json.dump(quizz_entries, output, indent = 4, ensure_ascii = False)
