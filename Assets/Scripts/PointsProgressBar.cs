﻿using System;
using System.Collections;
using System.Collections.Generic;
using BN512.Extensions;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// 
/// </summary>
public class PointsProgressBar : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    public GameMode gameMode;

    public Image progressBarFill;

    public CanvasGroup canvasGroup;

    public TMP_Text label;

    public string template = "{0}/{1}";

    public int currentFillStep;

    public float duration = .5f;

    [ShowInInspector, PropertyRange(0, 5)]
    public int FillStep
    {
        get { return currentFillStep; }

        set
        {
            var max = gameMode.config.Data.maxScore - 1;
            // print(max);
            var step = Mathf.Clamp(value, 0, max);
            if(currentFillStep != step)
            {
                currentFillStep = step;
                var fillAmount = (1.0f / max) * currentFillStep;


                if (Application.isEditor && !Application.isPlaying)
                {
                    progressBarFill.fillAmount = fillAmount;
                }
                else
                {
                    progressBarFill.DOFillAmount(fillAmount, duration);
                }

                label.text = template.FormatWith(step, max);
            }
        }
    }

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    #endregion

    #region Methods



    #endregion

}
